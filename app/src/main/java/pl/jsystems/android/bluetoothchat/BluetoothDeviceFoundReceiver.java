package pl.jsystems.android.bluetoothchat;

import java.util.Map;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BluetoothDeviceFoundReceiver extends BroadcastReceiver {
    private final Map<String, BluetoothDevice> devices;

    public BluetoothDeviceFoundReceiver(Map<String, BluetoothDevice> devices) {
        this.devices = devices;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        devices.put(device.getAddress(), device);
    }
}
