package pl.jsystems.android.bluetoothchat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements DevicesAdapter.DeviceClickListener {
    private RecyclerView rvDevices;
    private BluetoothAdapter btAdapter;
    private final Map<String, BluetoothDevice> devices = new HashMap<>();
    private final BluetoothScanStartedReceiver scanStartedReceiver =
            new BluetoothScanStartedReceiver();
    private final BluetoothDeviceFoundReceiver deviceFoundReceiver =
            new BluetoothDeviceFoundReceiver(devices);
    private final BluetoothScanFinishedReceiver scanFinishedReceiver =
            new BluetoothScanFinishedReceiver(new Runnable() {
                @Override
                public void run() {
                    displayDevices();
                    Intent serverIntent = new Intent(MainActivity.this,
                            ChatServerService.class);
                    startService(serverIntent);
                }
            });


    private void displayDevices() {
        DevicesAdapter adapter = new DevicesAdapter(this);

        List<BtDevice> deviceList = new ArrayList<>();
        for (BluetoothDevice systemDevice : devices.values()) {
            BtDevice device = new BtDevice();
            device.setName(systemDevice.getName());
            device.setMac(systemDevice.getAddress());
            device.setStatus(systemDevice.getBondState() == BluetoothDevice.BOND_BONDED ? "Sparowane" : "Nie sparowane");
            deviceList.add(device);
        }

        adapter.setDevices(deviceList);
        rvDevices.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (100 == requestCode) {
            if (resultCode == RESULT_OK) {
                discoverBluetoothDevices();
            } else {
                Toast.makeText(this, "Bez Bluetooth nie działam!",
                        Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(scanStartedReceiver);
        unregisterReceiver(deviceFoundReceiver);
        unregisterReceiver(scanFinishedReceiver);
    }

    private void discoverBluetoothDevices() {
        registerReceiver(scanStartedReceiver,
                new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED));
        registerReceiver(deviceFoundReceiver,
                new IntentFilter(BluetoothDevice.ACTION_FOUND));
        registerReceiver(scanFinishedReceiver,
                new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));
        btAdapter.startDiscovery();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvDevices = findViewById(R.id.rv_devices);
        btAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!btAdapter.isEnabled()) {
            Intent btIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(btIntent, 100);
        } else {
            discoverBluetoothDevices();
        }
    }

    @Override
    public void onDeviceClick(BtDevice device) {
        Intent chatIntent = new Intent(this, ChatActivity.class);
        chatIntent.putExtra("address", device.getMac());
        startActivity(chatIntent);
    }
}
