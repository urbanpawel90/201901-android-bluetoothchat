package pl.jsystems.android.bluetoothchat;

import java.util.List;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DevicesAdapter extends RecyclerView.Adapter<DevicesAdapter.DeviceViewHolder> {
    private List<BtDevice> devices;
    private DeviceClickListener clickListener;

    public DevicesAdapter(DeviceClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public List<BtDevice> getDevices() {
        return devices;
    }

    public void setDevices(List<BtDevice> devices) {
        this.devices = devices;
    }

    @NonNull
    @Override
    public DeviceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View rowView = inflater.inflate(R.layout.row_device, viewGroup, false);
        return new DeviceViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceViewHolder deviceViewHolder, int i) {
        BtDevice device = devices.get(i);

        deviceViewHolder.tvName.setText(device.getName());
        deviceViewHolder.tvMac.setText(device.getMac());
        deviceViewHolder.tvStatus.setText(device.getStatus());
        deviceViewHolder.currentDevice = device;
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    public class DeviceViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName, tvMac, tvStatus;
        private BtDevice currentDevice;

        public DeviceViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tv_name);
            tvMac = itemView.findViewById(R.id.tv_mac);
            tvStatus = itemView.findViewById(R.id.tv_status);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onDeviceClick(currentDevice);
                }
            });
        }
    }

    public interface DeviceClickListener {
        void onDeviceClick(BtDevice device);
    }
}
