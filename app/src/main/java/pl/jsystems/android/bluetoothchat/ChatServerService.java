package pl.jsystems.android.bluetoothchat;

import java.util.UUID;

import android.app.IntentService;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

public class ChatServerService extends IntentService {
    public static final UUID BT_UUID = UUID.fromString("e25c3c2f-9f5d-4ca4-81c1-6652907358b3");

    public static BluetoothSocket connectedSocket;

    public ChatServerService() {
        super("ChatServerService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try (BluetoothServerSocket serverSocket = BluetoothAdapter.getDefaultAdapter().listenUsingRfcommWithServiceRecord("Chat", BT_UUID)) {
            BluetoothSocket socket = serverSocket.accept();
            connectedSocket = socket;

            Log.d("ChatServerService", String.format("Connected %s (%s)",
                    socket.getRemoteDevice().getAddress(),
                    socket.getRemoteDevice().getName()));

            Intent chatIntent = new Intent(this, ChatActivity.class);
            chatIntent.putExtra("useSocket", true);
            chatIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(chatIntent);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
