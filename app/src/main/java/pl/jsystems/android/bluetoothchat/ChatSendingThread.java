package pl.jsystems.android.bluetoothchat;

import java.io.OutputStream;
import java.util.concurrent.LinkedBlockingQueue;

import android.bluetooth.BluetoothSocket;

public class ChatSendingThread extends Thread {
    private final BluetoothSocket socket;
    private final LinkedBlockingQueue<String> queue = new LinkedBlockingQueue<>();

    public ChatSendingThread(BluetoothSocket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            OutputStream os = socket.getOutputStream();

            while (true) {
                String message = queue.take();
                message = message + "\n";

                os.write(message.getBytes("UTF-8"));
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message) {
        queue.offer(message);
    }
}
