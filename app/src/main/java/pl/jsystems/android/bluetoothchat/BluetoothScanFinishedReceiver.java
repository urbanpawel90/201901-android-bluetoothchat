package pl.jsystems.android.bluetoothchat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BluetoothScanFinishedReceiver extends BroadcastReceiver {
    private final Runnable finishAction;

    public BluetoothScanFinishedReceiver(Runnable finishAction) {
        this.finishAction = finishAction;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        finishAction.run();
    }
}
