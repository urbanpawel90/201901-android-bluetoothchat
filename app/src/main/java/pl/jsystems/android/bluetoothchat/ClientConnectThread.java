package pl.jsystems.android.bluetoothchat;

import java.io.IOException;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

public class ClientConnectThread extends Thread {
    private final BluetoothDevice device;
    private BluetoothSocket socket;
    private ChatSendingThread sendingThread;

    public ClientConnectThread(BluetoothDevice device) {
        this.device = device;
    }

    public ClientConnectThread(BluetoothSocket connectedSocket) {
        this.socket = connectedSocket;
        this.device = null;
    }

    @Override
    public void run() {
        if (device == null && socket != null) {
            sendingThread = new ChatSendingThread(socket);
            sendingThread.start();
            return;
        }
        try {
            socket = device.createRfcommSocketToServiceRecord(ChatServerService.BT_UUID);
            socket.connect();
            Log.d("ClientConnectThread", String.format("Server %s (%s)",
                    socket.getRemoteDevice().getAddress(),
                    socket.getRemoteDevice().getName()));

            sendingThread = new ChatSendingThread(socket);
            sendingThread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        if (sendingThread != null) {
            sendingThread.interrupt();
        }
        if (socket != null && socket.isConnected()) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendMessage(String message) {
        if (sendingThread != null) {
            sendingThread.sendMessage(message);
        }
    }
}
