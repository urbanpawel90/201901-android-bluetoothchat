package pl.jsystems.android.bluetoothchat;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ChatActivity extends AppCompatActivity {
    private TextView tvChat;
    private EditText etMessage;
    private Button btnSend;

    private BluetoothAdapter btAdapter;
    private BluetoothDevice btDevice;
    private ClientConnectThread connectThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        tvChat = findViewById(R.id.tv_chat);
        etMessage = findViewById(R.id.et_message);
        btnSend = findViewById(R.id.btn_send);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMessageSendClick();
            }
        });

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        findBluetoothDevice();
    }

    private void onMessageSendClick() {
        String message = etMessage.getText().toString();
        if (message.trim().isEmpty()) {
            return;
        }

        etMessage.getText().clear();
        String logContent = tvChat.getText() + "\n[Ja]: " + message;
        tvChat.setText(logContent);

        connectThread.sendMessage(message);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (connectThread != null) {
            connectThread.disconnect();
        }
    }

    private void findBluetoothDevice() {
        boolean useServerSocket = getIntent().getBooleanExtra("useSocket", false);
        if (!useServerSocket) {
            String btAddress = getIntent().getStringExtra("address");
            btDevice = btAdapter.getRemoteDevice(btAddress);

            connectThread = new ClientConnectThread(btDevice);
            connectThread.start();
        } else {
            connectThread = new ClientConnectThread(ChatServerService.connectedSocket);
            connectThread.start();
        }
    }
}
